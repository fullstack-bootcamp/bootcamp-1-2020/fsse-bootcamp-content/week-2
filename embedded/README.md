# Embedded Domain: Vehicle System with SUMO

Make sure to [install sumo](https://gitlab.com/fullstack-bootcamp/bootcamp-1-2020/fsse-bootcamp-content/resources/-/blob/master/install_sumo.md)

Once you install SUMO and gave it running on your local machine (not inside your remote container!!!), you can verify it works by typing `sumo-gui` in your host (windows, mac or Linux) terminal.

Next, to install TraCI (SUMO control library), make a virtual environment:

```
virtualenv venv
```

and install the dependencies:

```
pip install -r requirements.txt
```

then run it:

```
python vehicle.py
```

It should open SUMO and run your vehicle. Make sure to have your cloud running!
