from time import time, sleep
import os
import sys
import requests

tools = os.path.join(os.environ['SUMO_HOME'], 'share/sumo/tools')
sys.path.append(tools)

import traci

sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "first_network.sumocfg",
           "--start", "--time-to-teleport", "100000000"]


# Vehicle comes off manufacturing line
VIN = "sumo_car"
veh = requests.post("http://localhost:9500/provision/"+VIN)

# save the assigned ID (uuid)
vehicle = veh.json()
print(vehicle)
vehicle_id = vehicle["vehicleId"]

traci.start(sumoCmd)

traci.vehicle.add(vehicle_id, "route0")

step = 1
while step < 10000:
    traci.simulationStep()

    speed = traci.vehicle.getSpeed(vehicle_id)
    out = {
        "vehicleSpeed": speed
    }
    
    requests.post(f"http://localhost:9500/vehicles/{vehicle_id}/data", json=out)
    print(speed)

    step += 1

    sleep(0.5)
    