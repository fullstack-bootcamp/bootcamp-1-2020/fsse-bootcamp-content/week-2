# Python Client

This represents a simple client of our connected vehicle system.

## Installation

Set up your virtual env:

```
virtualenv venv
```

This will create a venv folder that will store all your dependencies.

Then activate the virtual environment:

```
source venv/bin/activate
```

and finally, install the dependencies (only `requests` right now!).

```
pip install -r requirements.txt
```

> The requirements.txt file contains all the dependencies that are needed.

## Running the client

Make sure your cloud (server) is running, and simply do:

```
python main.py
```
