const express = require("express")
const morgan = require("morgan")
const bodyParser = require("body-parser")
const uuid = require("uuid")
const PORT = 9500

const app = express()

app.use(morgan("tiny"))
app.use(bodyParser.json())

let vehicles = { "4740c481-efe8-4a10-b6af-381c2398c5c0": { vin: "sumo_car", data: [] } }


app.post("/provision/:vin", (req, res) => {
    let id = uuid.v4()
    vehicles[id] = { vin: req.params.vin, data: [] }
    res.json({ vehicleId: id })
})

app.get("/vehicles/:vehicleId", (req, res) => {
    res.json(vehicles[req.params.vehicleId])
})

app.post("/vehicles/:vehicleId/data", (req, res) => {
    vehicles[req.params.vehicleId].data.push(req.body)
    console.log(vehicles[req.params.vehicleId].data)
    res.json()
})

app.get("/health-check", (req, res) => {
    res.json({ status: "OK!" })
})

app.listen(PORT, () => {
    console.log(`Cloud listening on ${PORT}`)
})
