# NodeJS Cloud

This represents a simple webserver ("cloud") with which your vehicle system will be communicating.

## Installation

In the `cloud` folder:

```
npm install
```

This will install all the dependencies in your `package.json` file.

## Running the server

Make sure your cloud (server) is running, and simply do:

```
node index.js
```

You should see `Cloud listening on 9500`!

If you would like to reload the server every time you make a change:

```
nodemon index.js
```
