const { expect } = require("chai");
const superagent = require("superagent");

const baseUrl = "http://localhost:9500";

describe("Vehicle Routes", () => {
  it("should provision a new vehicle", async () => {
    const res = await superagent
      .post(`${baseUrl}/provision/my_vehicle_id`)
      .send();
    expect(res.body).to.haveOwnProperty("vehicleId");
  });

  it("should get new vehicle", async () => {
    const newVehicle = await superagent
      .post(`${baseUrl}/provision/my_vehicle_id`)
      .send();
    expect(newVehicle.body).to.haveOwnProperty("vehicleId");

    const res = await superagent.get(
      `${baseUrl}/vehicles/${newVehicle.body.vehicleId}`
    );
    expect(res.body).to.haveOwnProperty("vin");
  });
});
